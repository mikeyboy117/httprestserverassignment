//
//  ViewController.m
//  HTTP REST Server Assignment
//
//  Created by Michael Childs on 3/3/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "ViewController.h"
#import "StudentDetailViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *studentTableView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshTableBtn;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    StudentDetailViewController* sdvc = [StudentDetailViewController new];
    self.studentTableView.delegate = self;
    self.studentTableView.dataSource = self;
    
    [self get];
    
    
}


-(void)get {
    NSURLSession* session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:nil];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://adamsparsetest.herokuapp.com/parse/classes/Game"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    NSError* error;
    
//    Use this vvvvvv for "POST" method
//    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:studentDictionary options:NSJSONWritingPrettyPrinted error:&error];
    
    
//    USE THIS vvvv for "GET" method
    
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"adamsappid" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setHTTPMethod:@"GET"];
    
    
    NSURLSessionDataTask* getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
        NSString* responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        NSDictionary* studentDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        
        studentArray = studentDictionary[@"results"];
        
        [self.studentTableView reloadData];
        
        NSLog(@"test");
    }];
    [getDataTask resume];
    
                        
    
    
                        
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self get];
}
- (IBAction)refreshData:(id)sender {
    [self get];
}

-(void)loadData {
//    AppDelegate* ad = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//    studentArray = ad.studentDictionary;
    
    
    [self.studentTableView reloadData];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }
    
    NSDictionary* studentD = studentArray[indexPath.row];
    
    
    
    
    
//     = [studentArray objectAtIndex:indexPath.row];
//
    cell.textLabel.text = [studentD objectForKey:@"name"];
    cell.detailTextLabel.text = [studentD objectForKey:@"color"];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return studentArray.count;
}



- (IBAction)addStudentBtn:(UIBarButtonItem *)sender {
    
    StudentDetailViewController* sdvc = [StudentDetailViewController new];
    
    [self presentViewController:sdvc animated:YES completion:NULL];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
