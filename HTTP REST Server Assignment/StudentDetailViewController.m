//
//  StudentDetailViewController.m
//  HTTP REST Server Assignment
//
//  Created by Michael Childs on 3/3/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "StudentDetailViewController.h"

@interface StudentDetailViewController ()<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *studentNameTF;
@property (strong, nonatomic) IBOutlet UIButton *postDataBtn;
@property (strong, nonatomic) IBOutlet UITextField *colorTF;

@end

@implementation StudentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    
    self.studentNameTF.placeholder = @"Enter Name";
    self.studentNameTF.delegate = self;
    self.studentNameTF.keyboardAppearance = UIKeyboardAppearanceDark;
    self.studentNameTF.autocapitalizationType = YES;
    self.postDataBtn.backgroundColor = [UIColor grayColor];
    self.colorTF.delegate = self;
    
}

- (IBAction)postData:(id)sender {
    
    
    
    NSURLSession* session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:nil];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://adamsparsetest.herokuapp.com/parse/classes/Game"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
    
    NSError* error;
    NSDictionary* d = @{@"name":self.studentNameTF.text, @"color":self.colorTF.text};
    
    //    Use this vvvvvv for "POST" method
        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:d options:NSJSONWritingPrettyPrinted error:&error];
    
    
    //    USE THIS vvvv for "GET" method
    
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"adamsappid" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setHTTPBody:jsonData];
    [request setHTTPMethod:@"POST"];
    
    
    NSURLSessionDataTask* getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
        NSString* responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.navigationController popViewControllerAnimated:YES];
            
            NSLog(@"Im on the main thread");
        });
        
        
        NSLog(@"test");
    }];
    [getDataTask resume];
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
